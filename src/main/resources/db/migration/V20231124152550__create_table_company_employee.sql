create table if not exists company (
                                       id   int auto_increment primary key,
                                       name varchar(255) null
);


CREATE TABLE IF NOT EXISTS employee
(
    id         INT auto_increment PRIMARY KEY,
    age        INT NULL,
    company_id INT NULL,
    gender     VARCHAR (255) NULL,
    name       VARCHAR (255) NULL,
    salary     INT NULL,
    FOREIGN KEY ( company_id ) REFERENCES company ( id )
);